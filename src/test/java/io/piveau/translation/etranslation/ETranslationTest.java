package io.piveau.translation.etranslation;

import io.piveau.translation.utils.Constants;
import io.piveau.translation.utils.ExternalReference;
import io.piveau.utils.Piveau;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisplayName("Testing eTranslation service")
@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ETranslationTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @BeforeAll
    public void setUp(Vertx vertx, VertxTestContext testContext) {
        testContext.completeNow();
    }

    @Test
    @Disabled("Uses external dependency")
    public void translationRequestTest(Vertx vertx, VertxTestContext testContext) {
        ExternalReference reference = new ExternalReference(
                "requestId",
                "subjectId",
                "propertyId"
        );

        JsonObject body = new JsonObject()
                .put("domain", "GEN")
                .put("callerInformation", new JsonObject()
                        .put("application", "PANEODP_20150429")
                        .put("username", "n00251qh"))
                .put("requesterCallback", "https://ppe.data.europa.eu/api/hub/translations/et_success_handler")
                .put("errorCallback", "https://ppe.data.europa.eu/api/hub/translations/et_error_handler")
                .put("externalReference", reference.getExternalReference())
                .put("textToTranslate", "This is a test")
                .put("sourceLanguage", "en")
                .put("targetLanguages", new JsonArray().add("de"));

        WebClient client = WebClient.create(vertx);

        Piveau.sendBufferDigestAuth(
                        client.postAbs("https://webgate.ec.europa.eu/etranslation/si/translate").putHeader("Content-Type", "application/json"),
                        "PANEODP_20150429",
                        "zRATVQDjG52mWb2Q",
                        body.toBuffer())
                .onSuccess(response -> {
                    Integer id = Integer.parseInt(response.bodyAsString());
                    if (id < 0) {
                        log.info("Request rejected: {} ({})", id, Constants.errorCodes.get(id));
                    } else {
                        log.info("Request accepted: {}", id);
                    }
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

}
