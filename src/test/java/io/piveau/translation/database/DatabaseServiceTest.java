package io.piveau.translation.database;

import io.piveau.translation.utils.Constants;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@DisplayName("Testing database functionality")
@ExtendWith(VertxExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
@Disabled("No db available")
class DatabaseServiceTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private DatabaseService databaseService;

    @BeforeAll
    public void setUp(Vertx vertx, VertxTestContext testContext) {
        DeploymentOptions options = new DeploymentOptions().setConfig(new JsonObject()
                .put(Constants.DATABASE, new JsonObject()
                                .put("host", "localhost")
                                .put("port", 5433)
                                .put("db_name", "translation_test")
                                .put("user", "")
                                .put("password", "")
                ));
        vertx.deployVerticle(DatabaseServiceVerticle.class, options)
                .onSuccess(id -> {
                    databaseService = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    void insertTranslationRequest(Vertx vertx, VertxTestContext testContext) {
        vertx.fileSystem().readFile("translationRequest.json")
                .compose(buffer -> databaseService.insertTranslationRequest(buffer.toJsonObject()))
                .compose(v -> databaseService.status())
                .onSuccess(status -> {
                    log.debug(status.encodePrettily());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Disabled("Preparation missing")
    void getTranslationRequest(Vertx vertx, VertxTestContext testContext) {

    }

}
