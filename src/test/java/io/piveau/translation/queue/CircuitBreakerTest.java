package io.piveau.translation.queue;

import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.circuitbreaker.CircuitBreakerState;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisplayName("Testing circuit breaker functionality")
@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CircuitBreakerTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private CircuitBreaker circuitBreaker;

    @BeforeAll
    void setUp(Vertx vertx, VertxTestContext testContext) {
        circuitBreaker = CircuitBreaker.create("test", vertx, new CircuitBreakerOptions()
                        .setMaxFailures(3)
                        .setResetTimeout(2000))
                .halfOpenHandler(v -> log.info("Half opened"))
                .openHandler(v -> log.info("Opened"))
                .closeHandler(v -> log.info("Closed"))
                .fallback(cause -> {
                    log.error("Fallback", cause);
                    return "fallback";
                });

        testContext.completeNow();
    }

    @Test
    void failures(Vertx vertx, VertxTestContext testContext) throws InterruptedException {
        int i = 1;
        while(i < 5) {
            if (circuitBreaker.state() != CircuitBreakerState.OPEN) {
                final int count = i++;
                circuitBreaker.execute(promise -> {
                            if (count < 4) {
                                promise.fail("failure triggered");
                            } else {
                                promise.complete("success triggered");
                            }
                        })
                        .onSuccess(value -> {
                            log.info("Success: {}", value);
                        })
                        .onFailure(cause -> log.error("Failure", cause));

                Thread.sleep(500);
            }
        }
        testContext.completeNow();
    }

}