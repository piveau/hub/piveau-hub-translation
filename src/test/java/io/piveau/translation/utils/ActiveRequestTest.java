package io.piveau.translation.utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ActiveRequestTest {

    private ActiveRequest activeRequest;

    @BeforeEach
    public void setUp() {
        JsonObject content = new JsonObject()
                .put("trid", "http://data.europa.eu/88u/dataset/any-dataset")
                .put("data", new JsonObject()
                        .put("catalogueId", "catalogue")
                        .put("defaultLanguage", "en"))
                .put("dict", new JsonObject()
                        .put("http://data.europa.eu/88u/dataset/any-distribution", new JsonObject()
                                .put("http://purl.org/dc/terms/title", new JsonObject()
                                        .put("sourceLanguage", "en")
                                        .put("targetLanguages", new JsonArray().add("fr").add("de"))
                                        .put("value", "This is a test.")
                                )
                        )
                )
                .put("callback", new JsonObject()
                        .put("url", "anyUrl")
                        .put("headers", new JsonObject()
                                .put("Authorization", "anyKey")
                        )
                );

        activeRequest = new ActiveRequest(content);
    }

    @Test
    void printJson() {
        activeRequest.snippets().forEach(snippet -> {
            snippet.add("fr", "C'est un test");
            snippet.fail(-9001, new JsonArray().add("de"));
        });
        System.out.println(activeRequest.toJson().encodePrettily());
    }

}
