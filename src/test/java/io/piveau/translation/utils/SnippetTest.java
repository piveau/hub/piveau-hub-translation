package io.piveau.translation.utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

class SnippetTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private Snippet snippet;

    @BeforeEach
    public void setUp() {
        JsonObject content = new JsonObject()
                .put("targetLanguages", new JsonArray()
                        .add("de")
                        .add("fr"))
                .put("sourceLanguage", "en")
                .put("value", "That's life.");

        snippet = new Snippet(
                null,
                "requestId",
                "subjectId",
                "propertyId",
                content);
    }

    @Test
    void snippetReference() {
        assertEquals("requestId+++subjectId+++propertyId", snippet.getReference().getExternalReference());
    }

    @Test
    void incompleteSnippet() {
        snippet.add("fr", "C'est la vie.");
        log.debug(snippet.status().encodePrettily());
        assertFalse(snippet.isComplete());
    }

    @Test
    void completeSnippet() {
        snippet.add("fr", "C'est la vie.");
        snippet.add("de", "So spielt das Leben.");
        log.debug(snippet.status().encodePrettily());
        assertTrue(snippet.isComplete());
    }

    @Test
    void failedSnippet() {
        snippet.add("fr", "C'est la vie.");
        snippet.fail(-9001, new JsonArray("[\"de\"]"));
        log.debug(snippet.status().encodePrettily());
        assertTrue(snippet.isComplete());
    }

}
