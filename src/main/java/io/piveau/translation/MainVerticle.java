package io.piveau.translation;

import io.piveau.json.ConfigHelper;
import io.piveau.translation.database.DatabaseService;
import io.piveau.translation.queue.TranslationQueueService;
import io.piveau.translation.request.TranslationRequestService;
import io.piveau.translation.request.TranslationRequestVerticle;
import io.piveau.translation.database.DatabaseServiceVerticle;
import io.piveau.translation.queue.TranslationQueueServiceVerticle;
import io.piveau.translation.utils.Constants;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    private TranslationQueueService queueService;
    private TranslationRequestService requestService;
    private DatabaseService databaseService;

    @Override
    public void start(Promise<Void> startPromise) {
        log.info("Starting translation service");

        queueService = TranslationQueueService.createProxy(vertx, TranslationQueueService.ADDRESS);
        requestService = TranslationRequestService.createProxy(vertx, TranslationRequestService.SERVICE_ADDRESS);
        databaseService = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);

        ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
                .setType("file")
                .setOptional(true)
                .setConfig(new JsonObject().put("path", "conf/translation-config.json"));

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject()
                        .put("keys", new JsonArray()
                                .add(Constants.DATABASE)
                                .add(Constants.ETRANSLATION)
                                .add(Constants.TRANSLATION_SERVICE))
                );

        // Order is important here, last config store overwrites all rules before (envStore > fileStore)
        ConfigRetrieverOptions options = new ConfigRetrieverOptions()
                .addStore(fileStoreOptions)
                .addStore(envStoreOptions);
        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);

        // Defining verticles and give them the configuration
        retriever.getConfig()
                .compose(config -> {
                    if (log.isDebugEnabled()) {
                        log.debug(config.encodePrettily());
                    }

                    return Future.<JsonObject>future(pr -> {
                        DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(config);
                        vertx.deployVerticle(DatabaseServiceVerticle.class, deploymentOptions)
                                .compose(id -> vertx.deployVerticle(TranslationRequestVerticle.class, deploymentOptions))
                                .compose(id -> vertx.deployVerticle(TranslationQueueServiceVerticle.class, deploymentOptions))
                                .onSuccess(f -> pr.complete(config))
                                .onFailure(pr::fail);
                    });
                })
                .compose(config -> {
                    HttpServerOptions httpServerOptions = new HttpServerOptions()
                            .setMaxHeaderSize(32 * 1024)
                            .setMaxFormAttributeSize(-1);
                    HttpServer server = vertx.createHttpServer(httpServerOptions);

                    Router router = Router.router(vertx);
                    router.post().handler(BodyHandler.create());

                    router.post("/translation-service").handler(this::handleTranslationRequest);
                    router.post("/et_success_handler").handler(this::handleTranslationSuccess);
                    router.get("/et_success_handler").handler(this::handleTranslationSuccess);
                    router.post("/et_error_handler").handler(this::handleTranslationError);
                    router.get("/et_error_handler").handler(this::handleTranslationError);
                    router.get("/status").handler(this::handleServiceStatus);
                    router.get("/translations").handler(this::handleGetTranslation);

                    ConfigHelper configHelper = ConfigHelper.forConfig(config());
                    int port = configHelper.forceJsonObject(Constants.TRANSLATION_SERVICE).getInteger("port", 8080);

                    return server.requestHandler(router).listen(port);
                })
                .onSuccess(server -> startPromise.complete())
                .onFailure(startPromise::fail);
    }

    private void handleServiceStatus(RoutingContext context) {
        Boolean verbose = Boolean.valueOf(context.queryParams().get("verbose"));

        queueService.status(verbose)
                .compose(status -> Future.<JsonObject>future(promise ->
                                databaseService.status()
                                        .onSuccess(dbStatus ->
                                                promise.complete(new JsonObject()
                                                        .put("queues", status)
                                                        .put("db", dbStatus))
                                        )
                                        .onFailure(promise::fail)
                        )
                )
                .onSuccess(status ->
                        context.response()
                                .setStatusCode(200)
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .end(status.encodePrettily())
                )
                .onFailure(context::fail);
    }

    private void handleTranslationRequest(RoutingContext context) {
        JsonObject translationRequest = context.getBodyAsJson();
        requestService.translationRequest(translationRequest)
                .onSuccess(status -> context.response().setStatusCode(204).end())
                .onFailure(cause -> {
                    if (cause instanceof ServiceException) {
                        ServiceException serviceException = (ServiceException) cause;
                        context.response()
                                .setStatusCode(serviceException.failureCode())
                                .end(serviceException.getMessage());
                    } else {
                        context.fail(500, cause);
                    }
                });
    }

    private void handleTranslationSuccess(RoutingContext context) {

        MultiMap attributes = context.request().isExpectMultipart() ?
                context.request().formAttributes() : context.queryParams();

        Integer requestId = Integer.valueOf(attributes.get("request-id"));
        String externalReference = attributes.get("external-reference");
        String translatedText = attributes.get("translated-text");
        String targetLanguage = attributes.get("target-language");

        if (externalReference != null && targetLanguage != null && translatedText != null) {
            queueService.translationSuccess(requestId, externalReference, targetLanguage.toLowerCase(), translatedText.trim());
            context.response().setStatusCode(204).end();
        } else {
            log.error("Attribute missing: {}, {}, {}", externalReference, targetLanguage, translatedText);
            context.response().setStatusCode(400).end();
        }
    }

    private void handleTranslationError(RoutingContext context) {

        MultiMap attributes = context.request().isExpectMultipart() ?
                context.request().formAttributes() : context.queryParams();

        log.error("Attributes:\n{}", attributes);

        JsonObject error = new JsonObject()
                .put("requestId", Integer.valueOf(attributes.get("request-id")))
                .put("errorMessage", attributes.get("error-message"));
        if (attributes.contains("external-reference")) {
            error.put("externalReference", attributes.get("external-reference"));
        }
        if (attributes.contains("error-code")) {
            error.put("errorCode", Integer.valueOf(attributes.get("error-code")));
        }
        if (attributes.contains("target-languages[]")) {
            List<String> languages = attributes.getAll("target-languages[]")
                    .stream()
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());
            if (languages.size() > 1) {
                error.put("targetLanguages", new JsonArray(languages));
            } else {
                error.put("targetLanguages", new JsonArray(Arrays.asList(languages.get(0).split(","))));
            }
        }

        if (log.isErrorEnabled()) {
            log.error("Translation error received:\n{}", error.encodePrettily());
        }

        queueService.translationError(error);

        context.response().setStatusCode(204).end();
    }

    private void handleGetTranslation(RoutingContext context) {
        String requestId = context.queryParams().get("requestId");
        if (requestId == null || requestId.isBlank()) {
            context.response().setStatusCode(400).end("Missing query parameter 'requestId'");
        } else {
            databaseService.selectTranslationRequest(requestId)
                    .onSuccess(translation -> {
                        if (translation.isEmpty()) {
                            context.response().setStatusCode(404).end();
                        } else {
                            translation.put("id", translation.remove("trid"));
                            context.response()
                                    .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                                    .setStatusCode(200)
                                    .end(translation.encodePrettily());
                        }
                    })
                    .onFailure(cause -> context.response().setStatusCode(500).end(cause.getMessage()));
        }
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
