package io.piveau.translation.request;

import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ContentChecker {

    private ContentChecker() {
    }

    public static void cleanDataDict(JsonObject translationRequest) {
        JsonObject dataDict = translationRequest.getJsonObject("dict");
        List<String> faults = dataDict.stream()
                .filter(entry -> entry.getValue() == null || entry.getValue().toString().isBlank())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        faults.forEach(dataDict::remove);
    }

}
