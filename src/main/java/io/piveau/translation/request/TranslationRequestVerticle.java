package io.piveau.translation.request;

import io.piveau.json.ConfigHelper;
import io.piveau.translation.utils.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class TranslationRequestVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        JsonObject translationConfig = ConfigHelper.forConfig(config()).forceJsonObject(Constants.TRANSLATION_SERVICE);
        new ServiceBinder(vertx)
                .setAddress(TranslationRequestService.SERVICE_ADDRESS)
                .register(TranslationRequestService.class, TranslationRequestService.create(vertx, translationConfig));

        startPromise.complete();
    }

}
