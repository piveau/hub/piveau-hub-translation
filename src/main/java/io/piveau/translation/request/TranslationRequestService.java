package io.piveau.translation.request;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface TranslationRequestService {
    String SERVICE_ADDRESS = "io.piveau.translation.request.queue";

    static TranslationRequestService create(Vertx vertx, JsonObject config) {
        return new TranslationRequestServiceImpl(vertx, config);
    }

    static TranslationRequestService createProxy(Vertx vertx, String address) {
        return new TranslationRequestServiceVertxEBProxy(vertx, address);
    }

    Future<Void> translationCallback(JsonObject request);

    Future<String> translationRequest(JsonObject request);

}
