package io.piveau.translation.request;

import io.piveau.translation.database.DatabaseService;
import io.piveau.utils.PiveauContext;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TranslationRequestServiceImpl implements TranslationRequestService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final WebClient webClient;

    private final DatabaseService databaseService;

    private final List<String> acceptedLanguages;

    private final PiveauContext moduleContext = new PiveauContext("translation", "request");

    public TranslationRequestServiceImpl(Vertx vertx, JsonObject config) {
        webClient = WebClient.create(vertx);
        databaseService = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);
        acceptedLanguages = config.getJsonArray("accepted_languages", new JsonArray())
                .stream().map(Object::toString).collect(Collectors.toList());

        log.info("Translation request service started");
    }

    @Override
    public Future<Void> translationCallback(JsonObject request) {
        return Future.future(promise -> {
            String requestId = request.getString("id");
            PiveauContext resourceContext = moduleContext.extend(requestId);

            JsonObject callback = (JsonObject) request.remove("callback");
            String url = callback.getString("url", "");
            Map<String, String> headers = callback.getJsonObject("headers", new JsonObject()).getMap()
                    .entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> (String)e.getValue()));
            MultiMap callbackHeaders = MultiMap.caseInsensitiveMultiMap().setAll(headers);

            String method = callback.getString("method", "POST");
            webClient.requestAbs(HttpMethod.valueOf(method), url)
                    .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                    .putHeaders(callbackHeaders)
                    .sendJsonObject(request)
                    .onSuccess(response -> {
                        resourceContext.log().info("Translation returned");
                        resourceContext.log().debug(request.encodePrettily());
                        promise.complete();
                    })
                    .onFailure(cause -> {
                        resourceContext.log().error("Translation callback", cause);
                        promise.fail(cause);
                    });
        });
    }

    @Override
    public Future<String> translationRequest(JsonObject request) {
        return Future.future(promise -> {
            if (log.isDebugEnabled()) {
                log.debug(request.encodePrettily());
            }

            PiveauContext translationContext = moduleContext.extend(request.getString("id"));

            databaseService.insertTranslationRequest(request)
                    .onSuccess(v -> {
                        translationContext.log().info("Translation request accepted");
                        translationContext.log().debug(request.encodePrettily());
                        promise.complete("accepted");
                    })
                    .onFailure(cause -> {
                        translationContext.log().error("Translation storing", cause);
                        promise.fail(cause);
                    });
        });
    }

}
