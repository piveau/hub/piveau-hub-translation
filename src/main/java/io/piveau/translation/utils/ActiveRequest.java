package io.piveau.translation.utils;

import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class ActiveRequest {

    private final String id;
    private final JsonObject data;
    private final JsonObject dict;
    private final JsonObject callback;
    private final Instant startTime;

    private final Map<String, Snippet> snippets = new HashMap<>();

    public ActiveRequest(JsonObject row) {
        startTime = Instant.now();
        id = row.getString("trid");
        data = row.getJsonObject("data");
        dict = row.getJsonObject("dict");
        callback = row.getJsonObject("callback");

        // generate snippet map
        dict.forEach(subjectEntry -> {
            String subjectId = subjectEntry.getKey();
            JsonObject properties = (JsonObject) subjectEntry.getValue();
            properties.forEach(propertyEntry -> {
                Snippet snippet = new Snippet(
                        this,
                        id,
                        subjectId,
                        propertyEntry.getKey(),
                        (JsonObject) propertyEntry.getValue());
                snippets.put(snippet.getReference().getExternalReference(), snippet);
            });
        });
    }

    public String id() {
        return id;
    }

    public long getDuration() {
        return ChronoUnit.MINUTES.between(startTime, Instant.now());
    }

    public boolean isComplete() {
        return snippets.values().stream().allMatch(Snippet::isComplete);
    }

    public Collection<Snippet> snippets() {
        return snippets.values();
    }

    public JsonObject toJson() {
        JsonObject finalDict = dict.copy();
        // merge translations into dict
        snippets.values().stream()
                .map(Snippet::toJson)
                .filter(snippet -> finalDict.containsKey(snippet.getString("subjectId")))
                .forEach(snippet -> {
                    JsonObject subject = finalDict.getJsonObject(snippet.getString("subjectId"));
                    JsonObject property = subject.getJsonObject(snippet.getString("propertyId"));
                    property.put("translations", snippet.getJsonObject("translations"));
                });

        return new JsonObject()
                .put("id", id)
                .put("data", data)
                .put("dict", finalDict)
                .put("callback", callback);
    }

}
