package io.piveau.translation.utils;

import java.util.Map;

public final class Constants {

    private Constants() {
    }

    public static final String TRANSLATION_SERVICE = "TRANSLATION_SERVICE";
    public static final String DATABASE = "DATABASE";
    public static final String ETRANSLATION = "E_TRANSLATION";

    public static final String CONFIG_ETRANSLATION_USER = "user";
    public static final String CONFIG_ETRANSLATION_APP = "application";
    public static final String CONFIG_ETRANSLATION_PASSWORD = "password";
    public static final String CONFIG_ETRANSLATION_URL = "e_translation_url";
    public static final String CONFIG_ETRANSLATION_CALLBACK = "callback_url";
    public static final String CONFIG_ETRANSLATION_SIMULTANEOUS_TRANSLATIONS = "simultaneous_translations";
    public static final String CONFIG_ETRANSLATION_TRANSLATION_WAITINGTIME = "timeout";
    public static final String CONFIG_ETRANSLATION_REQUEST_INTERVAL = "request_interval";
    public static final String CONFIG_ETRANSLATION_SNIPPET_INTERVAL = "snippet_interval";
    public static final String CONFIG_ETRANSLATION_QUOTA = "quota";
    public static final String CONFIG_ETRANSLATION_KEEP_FINALIZED = "keep_finalized";

    public static final String CONFIG_PIVEAU_SHELL = "PIVEAU_SHELL_CONFIG";

    public static final Map<Integer, String> errorCodes = Map.<Integer, String>ofEntries(
            Map.entry(41003, "Translation timeout"),
            Map.entry(-20000, "Source language not specified"),
            Map.entry(-20001, "Invalid source language"),
            Map.entry(-20002, "Target language(s) not specified"),
            Map.entry(-20003, "Invalid target language(s)"),
            Map.entry(-20004, "DEPRECATED"),
            Map.entry(-20005, "Caller information not specified"),
            Map.entry(-20006, "Missing application name"),
            Map.entry(-20007, "Application not authorized to access the service"),
            Map.entry(-20008, "Bad format for ftp address"),
            Map.entry(-20009, "Bad format for sftp address"),
            Map.entry(-20010, "Bad format for http address"),
            Map.entry(-20011, "Bad format for email address"),
            Map.entry(-20012, "Translation request must be text type, document path type or document base64 type and not several at a time"),
            Map.entry(-20013, "Language pair not supported by the domain"),
            Map.entry(-20014, "Username parameter not specified"),
            Map.entry(-20015, "Extension invalid compared to the MIME type"),
            Map.entry(-20016, "DEPRECATED"),
            Map.entry(-20017, "Username parameter too long"),
            Map.entry(-20018, "Invalid output format"),
            Map.entry(-20019, "Institution parameter too long"),
            Map.entry(-20020, "Department number too long"),
            Map.entry(-20021, "Text to translate too long"),
            Map.entry(-20022, "Too many FTP destinations"),
            Map.entry(-20023, "Too many SFTP destinations"),
            Map.entry(-20024, "Too many HTTP destinations"),
            Map.entry(-20025, "Missing destination"),
            Map.entry(-20026, "Bad requester callback protocol"),
            Map.entry(-20027, "Bad error callback protocol"),
            Map.entry(-20028, "Concurrency quota exceeded"),
            Map.entry(-20029, "Document format not supported"),
            Map.entry(-20030, "Text to translate is empty"),
            Map.entry(-20031, "Missing text or document to translate"),
            Map.entry(-20032, "Email address too long"),
            Map.entry(-20033, "Cannot read stream"),
            Map.entry(-20034, "Output format not supported"),
            Map.entry(-20035, "Email destination tag is missing or empty"),
            Map.entry(-20036, "HTTP destination tag is missing or empty"),
            Map.entry(-20037, "FTP destination tag is missing or empty"),
            Map.entry(-20038, "SFTP destination tag is missing or empty"),
            Map.entry(-20039, "Document to translate tag is missing or empty"),
            Map.entry(-20040, "Format tag is missing or empty"),
            Map.entry(-20041, "The content is missing or empty"),
            Map.entry(-20042, "Source language defined in TMX file differs from request"),
            Map.entry(-20043, "Source language defined in XLIFF file differs from request"),
            Map.entry(-20044, "Output format is not available when quality estimate is requested. It should be blank or 'xslx'"),
            Map.entry(-20045, "Quality estimate is not available for text snippet"),
            Map.entry(-20046, "Document too big (>20Mb)"),
            Map.entry(-20047, "Quality estimation not available"),
            Map.entry(-40010, "Too many segments to translate"),
            Map.entry(-80004, "Cannot store notification file at specified FTP address"),
            Map.entry(-80005, "Cannot store notification file at specified SFTP address"),
            Map.entry(-80006, "Cannot store translated file at specified FTP address"),
            Map.entry(-80007, "Cannot store translated file at specified SFTP address"),
            Map.entry(-90000, "Cannot connect to FTP"),
            Map.entry(-90001, "Cannot retrieve file at specified FTP address"),
            Map.entry(-90002, "File not found at specified address on FTP"),
            Map.entry(-90007, "Malformed FTP address"),
            Map.entry(-90012, "Cannot retrieve file content on SFTP"),
            Map.entry(-90013, "Cannot connect to SFTP"),
            Map.entry(-90014, "Cannot store file at specified FTP address"),
            Map.entry(-90015, "Cannot retrieve file content on SFTP"),
            Map.entry(-90016, "Cannot retrieve file at specified SFTP address")
    );

}
