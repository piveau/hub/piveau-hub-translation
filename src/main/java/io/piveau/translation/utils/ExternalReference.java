package io.piveau.translation.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExternalReference {

    private static final Pattern referencePattern = Pattern.compile("^(.+)\\+\\+\\+(.+)\\+\\+\\+(.+)$");

    private final String reference;

    private final String requestId;
    private final String subjectId;
    private final String propertyId;

    public ExternalReference(String externalReference) {
        this.reference = externalReference;

        Matcher matcher = referencePattern.matcher(externalReference);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Wrong format");
        }
        requestId = matcher.group(1);
        subjectId = matcher.group(2);
        propertyId = matcher.group(3);
    }

    public ExternalReference(String requestId, String snippetId, String propertyId) {
        this.requestId = requestId;
        this.subjectId = snippetId;
        this.propertyId = propertyId;

        reference = requestId + "+++" + snippetId + "+++" + propertyId;
    }

    public static boolean isExternalReference(String value) {
        return referencePattern.matcher(value).matches();
    }

    public String getExternalReference() {
        return reference;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public String getPropertyId() {
        return propertyId;
    }

}
