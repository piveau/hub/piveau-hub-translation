package io.piveau.translation.utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class Snippet {

    private final ActiveRequest request;

    private final ExternalReference reference;

    private final Set<String> targetLanguages;

    private final Map<String, Object> translations = new HashMap<>();

    private final String sourceLanguage;
    private final String value;

    private Instant startTime;

    private int resetCounter = 0;

    public Snippet(ActiveRequest request, String requestId, String subjectId, String propertyId, JsonObject content) {
        this.request = request;

        reference = new ExternalReference(requestId, subjectId, propertyId);
        value = content.getString("value", "");
        sourceLanguage = content.getString("sourceLanguage", "");

        targetLanguages = content.getJsonArray("targetLanguages", new JsonArray())
                .stream().map(Object::toString).collect(Collectors.toSet());
    }

    public ActiveRequest getRequest() {
        return request;
    }

    public Snippet activate() {
        startTime = Instant.now();
        return this;
    }

    public long getDuration() {
        return startTime != null ? ChronoUnit.MINUTES.between(startTime, Instant.now()) : 0L;
    }

    public ExternalReference getReference() {
        return reference;
    }

    public String value() {
        return value;
    }

    public String sourceLanguage() {
        return sourceLanguage;
    }

    public Set<String> targetLanguages() {
        return targetLanguages;
    }

    public void reset() {
        translations.clear();
        resetCounter++;
    }

    public boolean isComplete() {
        return translations.keySet().containsAll(targetLanguages);
    }

    public void fail(int error, JsonArray languages) {
        if (languages.isEmpty()) {
            targetLanguages.stream()
                    .map(String.class::cast)
                    .forEach(language -> translations.putIfAbsent(language, error));
        } else {
            languages.stream()
                    .map(String.class::cast)
                    .forEach(language -> translations.put(language, error));
        }
    }

    public void add(String language, String text) {
        translations.put(language, text);
    }

    public JsonObject toJson() {
        return new JsonObject()
                .put("requestId", reference.getRequestId())
                .put("subjectId", reference.getSubjectId())
                .put("propertyId", reference.getPropertyId())
                .put("sourceLanguage", sourceLanguage)
                .put("value", value)
                .put("targetLanguages", new JsonArray(List.copyOf(targetLanguages)))
                .put("translations", JsonObject.mapFrom(translations));
    }

    public JsonObject status() {
        return new JsonObject()
                .put("duration", getDuration())
                .put("resets", resetCounter)
                .put("source-text", value)
                .put("source-language", sourceLanguage)
                .put("target-languages", new JsonArray(List.copyOf(targetLanguages)))
                .put("translations", JsonObject.mapFrom(translations));
    }

}
