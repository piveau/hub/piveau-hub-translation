package io.piveau.translation.shell;

import io.piveau.translation.database.DatabaseService;
import io.piveau.translation.queue.TranslationQueueService;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.cli.CLI;
import io.vertx.core.cli.Option;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.shell.command.Command;
import io.vertx.ext.shell.command.CommandBuilder;

public class StatusCommand {
    private final Command command;

    private final DatabaseService databaseService;
    private final TranslationQueueService queueService;

    private StatusCommand(Vertx vertx) {
        databaseService = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);
        queueService = TranslationQueueService.createProxy(vertx, TranslationQueueService.ADDRESS);

        command = CommandBuilder.command(
                CLI.create("status")
                        .setDescription("Print status information")
                        .addOption(new Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help"))
                        .addOption(new Option().setFlag(true).setArgName("verbose").setShortName("v").setLongName("verbose"))
        ).processHandler(process ->
            queueService.status(process.commandLine().isFlagEnabled("verbose"))
                    .compose(status -> Future.<JsonObject>future(promise ->
                                    databaseService.status()
                                            .onSuccess(dbStatus ->
                                                    promise.complete(new JsonObject()
                                                            .put("queues", status)
                                                            .put("db", dbStatus))
                                            )
                                            .onFailure(promise::fail)
                            )
                    )
                    .onSuccess(status -> process.write(status.encodePrettily() + "\n").end())
                    .onFailure(cause -> process.write(cause.getMessage() + "\n").end())
        ).build(vertx);
    }

    public static Command create(Vertx vertx) {
        return new StatusCommand(vertx).command;
    }

}
