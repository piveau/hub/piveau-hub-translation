package io.piveau.translation.queue;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface TranslationQueueService {
    String ADDRESS = "io.piveau.translation.queue.queue";

    static Future<TranslationQueueService> create(Vertx vertx, JsonObject config) {
        return Future.future(promise -> new TranslationQueueServiceImpl(vertx, config, promise));
    }

    static TranslationQueueService createProxy(Vertx vertx, String address) {
        return new TranslationQueueServiceVertxEBProxy(vertx, address);
    }

    Future<Void> translationSuccess(Integer requestId, String externalReference, String language, String translation);

    Future<Void> translationError(JsonObject error);

    Future<JsonObject> status(Boolean verbose);

}
