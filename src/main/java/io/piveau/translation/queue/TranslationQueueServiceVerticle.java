package io.piveau.translation.queue;

import io.piveau.json.ConfigHelper;
import io.piveau.translation.utils.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class TranslationQueueServiceVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        JsonObject config = ConfigHelper.forConfig(config()).forceJsonObject(Constants.ETRANSLATION);
        TranslationQueueService.create(vertx, config)
                .onSuccess(service -> {
                    new ServiceBinder(vertx)
                            .setAddress(TranslationQueueService.ADDRESS)
                            .register(TranslationQueueService.class, service);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);
    }

}
