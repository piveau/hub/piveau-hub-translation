package io.piveau.translation.queue;

import io.piveau.translation.database.DatabaseService;
import io.piveau.translation.request.TranslationRequestService;
import io.piveau.translation.utils.ActiveRequest;
import io.piveau.translation.utils.Constants;
import io.piveau.translation.utils.ExternalReference;
import io.piveau.translation.utils.Snippet;
import io.piveau.utils.Piveau;
import io.piveau.utils.PiveauContext;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.circuitbreaker.CircuitBreakerState;
import io.vertx.core.*;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class TranslationQueueServiceImpl implements TranslationQueueService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final Map<String, ActiveRequest> activeRequests = Collections.synchronizedMap(new HashMap<>());
    private final Map<Integer, Snippet> activeSnippets = Collections.synchronizedMap(new HashMap<>());

    private final Queue<Snippet> snippetQueue = new ConcurrentLinkedQueue<>();

    private int maxRequests = 5;
    private int requestTimeout = 0;

    private final AtomicInteger snippetCounter = new AtomicInteger();

    private final DatabaseService databaseService;
    private final TranslationRequestService requestService;

    private final PiveauContext eTranslationContext = new PiveauContext("translation", "eTranslation");
    private final PiveauContext moduleContext = new PiveauContext("translation", "queue");

    private int quota = 500;

    private final CircuitBreaker circuitBreaker;

    private final String url;
    private final JsonObject etTemplate = new JsonObject();
    private final String application;
    private final String password;

    private final boolean keepFinalized;

    private final WebClient client;

    TranslationQueueServiceImpl(Vertx vertx, JsonObject config, Promise<TranslationQueueService> promise) {

        client = WebClient.create(vertx, new WebClientOptions().setMaxPoolSize(200));

        url = config.getString(Constants.CONFIG_ETRANSLATION_URL, "https://webgate.ec.europa.eu/etranslation/si/translate");
        application = config.getString(Constants.CONFIG_ETRANSLATION_APP, "");
        password = config.getString(Constants.CONFIG_ETRANSLATION_PASSWORD, "");

        keepFinalized = config.getBoolean(Constants.CONFIG_ETRANSLATION_KEEP_FINALIZED, false);

        etTemplate
                .put("domain", "GEN")
                .put("callerInformation", new JsonObject()
                        .put("application", application)
                        .put("username", config.getString(Constants.CONFIG_ETRANSLATION_USER, "")))
                .put("requesterCallback", config.getString(Constants.CONFIG_ETRANSLATION_CALLBACK, "http://localhost:8080/translation-service") + "/et_success_handler")
                .put("errorCallback", config.getString(Constants.CONFIG_ETRANSLATION_CALLBACK, "http://localhost:8080/translation-service") + "/et_error_handler");

        maxRequests = config.getInteger(Constants.CONFIG_ETRANSLATION_SIMULTANEOUS_TRANSLATIONS, maxRequests);
        requestTimeout = config.getInteger(Constants.CONFIG_ETRANSLATION_TRANSLATION_WAITINGTIME, requestTimeout);
        quota = config.getInteger(Constants.CONFIG_ETRANSLATION_QUOTA, quota);

        databaseService = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);
        requestService = TranslationRequestService.createProxy(vertx, TranslationRequestService.SERVICE_ADDRESS);

        long requestInterval = config.getLong(Constants.CONFIG_ETRANSLATION_REQUEST_INTERVAL, 2000L);
        vertx.setPeriodic(requestInterval, this::activateRequests);

        // 200ms -> 5 requests per second
        long snippetInterval = config.getLong(Constants.CONFIG_ETRANSLATION_SNIPPET_INTERVAL, 200L);
        vertx.setPeriodic(snippetInterval, this::pullSnippets);

        if (requestTimeout > 0) {
            vertx.setPeriodic(60000, this::resetTimeouts);
        }

        circuitBreaker = CircuitBreaker.create(
                        "eTranslation",
                        vertx,
                        new CircuitBreakerOptions()
                                .setMaxFailures(2)
                                .setResetTimeout(60000)
                                .setTimeout(-1)
                )
                .halfOpenHandler(v -> log.debug("eTranslation circuit half opened"))
                .openHandler(v -> log.debug("eTranslation circuit opened"))
                .closeHandler(v -> log.debug("eTranslation circuit closed"));

        log.info("Translation queue service started");

        promise.complete(this);
    }

    @Override
    public Future<Void> translationSuccess(Integer requestId, String externalReference, String language, String text) {
        return Future.future(promise -> {
            PiveauContext resourceContext = eTranslationContext.extend(externalReference);

            if (activeSnippets.containsKey(requestId)) {
                resourceContext.log().info("Incoming translation \"{}\": \"{}\"", language, text);

                Snippet snippet = activeSnippets.get(requestId);
                snippet.add(language, text);

                checkFinalStatus(requestId, snippet, promise);
            } else {
                resourceContext.log().warn("No active request for translation");
                promise.complete();
            }
        });
    }

    @Override
    public Future<Void> translationError(JsonObject error) {
        return Future.future(promise -> {

            int id = error.getInteger("requestId");
            String externalReference = error.getString("externalReference", "");
            int errorCode = error.getInteger("errorCode", 0);
            JsonArray targetLanguages = error.getJsonArray("targetLanguages", new JsonArray());

            PiveauContext resourceContext = eTranslationContext.extend(externalReference);

            if (activeSnippets.containsKey(id)) {
                resourceContext.log().error("Translation error: {} {}", errorCode, targetLanguages.encode());

                Snippet snippet = activeSnippets.get(id);
                snippet.fail(errorCode, targetLanguages);

                checkFinalStatus(id, snippet, promise);
            } else {
                resourceContext.log().warn("No active request for translation error");
                promise.complete();
            }
        });
    }

    private void activateRequests(Long timerId) {
        if (activeRequests.size() < maxRequests) {
            databaseService.nextTranslationRequest()
                    .onSuccess(request -> {
                        if (!request.isEmpty()) {
                            ActiveRequest active = new ActiveRequest(request);
                            activeRequests.put(active.id(), active);
                            PiveauContext queueContext = moduleContext.extend(active.id());
                            queueContext.log().info("Start translation request");
                            active.snippets().forEach(snippetQueue::offer);
                        } else {
                            log.trace("No next request available");
                        }
                    })
                    .onFailure(cause -> log.error("Next request: {}", cause.getMessage()));
        }
    }

    private void resetTimeouts(Long timerId) {
        Map<Integer, Snippet> timedOut = activeSnippets.entrySet().stream()
                .filter(entry -> entry.getValue().getDuration() > requestTimeout)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        snippetCounter.set(snippetCounter.get() - timedOut.size());
        activeSnippets.keySet().removeAll(timedOut.keySet());

        timedOut.forEach((key, value) -> {
            value.reset();
            snippetQueue.add(value);
        });
    }

    private void pullSnippets(Long timerId) {
        if (snippetCounter.get() < quota && circuitBreaker.state() != CircuitBreakerState.OPEN) {
            Snippet snippet = snippetQueue.poll();
            if (snippet != null) {
                // Increment before sending. Otherwise, too many requests stacking up
                snippetCounter.incrementAndGet();
                circuitBreaker.<Integer>execute(promise ->
                                translationRequest(snippet)
                                        .compose(id -> {
                                            if (id == -20028) {
                                                return Future.failedFuture(Constants.errorCodes.get(id) + " (" + id + ")");
                                            } else {
                                                return Future.succeededFuture(id);
                                            }
                                        })
                                        .onComplete(promise)
                        )
                        .onSuccess(id -> {
                            if (id < 0) {
                                log.error("Translation request failure {}: {} ({})",
                                        snippet.getReference().getExternalReference(),
                                        Constants.errorCodes.get(id),
                                        id);

                                snippet.fail(id, new JsonArray());

                                snippetCounter.decrementAndGet();

                                ActiveRequest activeRequest = snippet.getRequest();
                                if (activeRequest.isComplete()) {
                                    PiveauContext queueContext = moduleContext.extend(activeRequest.id());
                                    queueContext.log().info("Translation request finished");
                                    finalizeActiveRequest(activeRequest);
                                }
                            } else {
                                log.debug("Translation id: {}", id);
                                activeSnippets.putIfAbsent(id, snippet.activate());
                            }
                        })
                        .onFailure(cause -> {
                            log.error("Translation request failure", cause);
                            snippet.reset();
                            snippetQueue.offer(snippet);
                            snippetCounter.decrementAndGet();
                        });
            }
        }
    }

    private Future<Void> finalizeActiveRequest(ActiveRequest request) {
        return Future.future(promise -> {
            activeRequests.remove(request.id());
            Future<Void> dbFuture;
            if (keepFinalized) {
                dbFuture = databaseService.finishTranslation(request.toJson());
            } else {
                dbFuture = databaseService.deleteTranslationRequest(request.id());
            }
            dbFuture.compose(v -> requestService.translationCallback(request.toJson())).onComplete(promise);
        });
    }

    @Override
    public Future<JsonObject> status(Boolean verbose) {
        return Future.future(promise -> {
            final AtomicLong incompleteSnippets = new AtomicLong(0);
            JsonObject requestDurations = new JsonObject();
            activeRequests.forEach((ref, request) -> {
                requestDurations.put(ref, request.getDuration());
                incompleteSnippets.getAndAdd(request.snippets().stream().filter(s -> !s.isComplete()).count());
            });
            JsonObject snippetDurations = new JsonObject();
            if (verbose) {
                activeSnippets.forEach((id, snippet) -> snippetDurations.put(id.toString(), snippet.status()));
            } else {
                activeSnippets.forEach((id, snippet) -> snippetDurations.put(id.toString(), snippet.getDuration()));
            }
            promise.complete(new JsonObject()
                    .put("requests-active", activeRequests.size())
                    .put("request-durations", requestDurations)
                    .put("snippet-durations", snippetDurations)
                    .put("snippets-queued", snippetQueue.size())
                    .put("snippets-incomplete", incompleteSnippets.intValue())
                    .put("snippets-active", activeSnippets.size())
                    .put("snippets-counter", snippetCounter.get()));
        });
    }

    private Future<Integer> translationRequest(Snippet snippet) {
        return Future.future(promise -> {
            ExternalReference reference = snippet.getReference();

            PiveauContext resourceContext = eTranslationContext.extend(reference.getExternalReference());

            JsonObject body = etTemplate.copy()
                    .put("externalReference", reference.getExternalReference())
                    .put("textToTranslate", snippet.value())
                    .put("sourceLanguage", snippet.sourceLanguage())
                    .put("targetLanguages", new JsonArray(List.copyOf(snippet.targetLanguages())));

            Piveau.sendBufferDigestAuth(
                            client
                                    .postAbs(url)
                                    .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json"),
                            application,
                            password,
                            body.toBuffer())
                    .onSuccess(response -> {
                        if (response.statusCode() >= 200 && response.statusCode() < 300) {
                            Integer id = Integer.parseInt(response.bodyAsString());
                            if (id < 0) {
                                resourceContext.log().info("Request rejected: {} ({})", Constants.errorCodes.get(id), id);
                            } else {
                                resourceContext.log().info("Request accepted: {}", id);
                            }
                            resourceContext.log().debug(body.encodePrettily());
                            promise.complete(id);
                        } else {
                            promise.fail(response.statusMessage());
                        }
                    })
                    .onFailure(promise::fail);
        });
    }

    private void checkFinalStatus(Integer requestId, Snippet snippet, Promise<Void> promise) {
        if (snippet.isComplete()) {
            snippetCounter.decrementAndGet();
            activeSnippets.remove(requestId);
        }

        ActiveRequest request = snippet.getRequest();
        if (request.isComplete()) {
            PiveauContext queueContext = moduleContext.extend(request.id());
            queueContext.log().info("Translation request finished");
            finalizeActiveRequest(request).onComplete(promise);
        } else {
            promise.complete();
        }
    }

}
