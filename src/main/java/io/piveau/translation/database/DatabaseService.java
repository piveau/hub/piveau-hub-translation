package io.piveau.translation.database;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface DatabaseService {
    String SERVICE_ADDRESS = "io.piveau.translation.database.queue";

    static DatabaseService create(Vertx vertx, JsonObject config, Handler<AsyncResult<DatabaseService>> readyHandler) {
        return new DatabaseServiceImpl(vertx, config, readyHandler);
    }

    static DatabaseService createProxy(Vertx vertx, String address) {
        return new DatabaseServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(360000));
    }

    Future<Void> insertTranslationRequest(JsonObject translationRequest);

    Future<JsonObject> nextTranslationRequest();

    Future<JsonObject> selectTranslationRequest(String trId);

    Future<Void> deleteTranslationRequest(String trId);

    Future<Void> resetTranslationRequest(String trId);

    Future<Void> finishTranslation(JsonObject request);

    Future<JsonObject> status();

}
