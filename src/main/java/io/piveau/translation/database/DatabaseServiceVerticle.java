package io.piveau.translation.database;

import io.piveau.json.ConfigHelper;
import io.piveau.translation.utils.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class DatabaseServiceVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        JsonObject dbConfig = ConfigHelper.forConfig(config()).forceJsonObject(Constants.DATABASE);
        DatabaseService.create(vertx, dbConfig, ar -> {
            if (ar.succeeded()) {
                new ServiceBinder(vertx)
                        .setAddress(DatabaseService.SERVICE_ADDRESS)
                        .register(DatabaseService.class, ar.result());
                startPromise.complete();
            } else {
                startPromise.fail(ar.cause());
            }
        });
    }

}
