package io.piveau.translation.database;

import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DatabaseServiceImpl implements DatabaseService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final SqlClient dbClient;

    private static final String CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS translation_request_all (trid TEXT, status TEXT, incoming TIMESTAMP, data JSON, dict JSON, callback JSON, PRIMARY KEY (trid))";
    private static final String INSERT_QUERY = "INSERT INTO translation_request_all VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (trid) DO UPDATE SET status = $2, incoming = $3, data = $4, dict = $5, callback = $6";
    private static final String NEXT_QUERY = "SELECT * FROM translation_request_all WHERE status = 'pending' ORDER BY incoming LIMIT 1";
    private static final String RESET_ALL_QUERY = "UPDATE translation_request_all SET status = 'pending' WHERE status = 'active' OR status = 'sent'";
    private static final String SET_ACTIVE_QUERY = "UPDATE translation_request_all SET status = 'active' WHERE trid = $1";
    private static final String SELECT_QUERY = "SELECT * FROM translation_request_all WHERE trid = $1";
    private static final String DELETE_QUERY = "DELETE FROM translation_request_all WHERE trid = $1";
    private static final String RESET_QUERY = "UPDATE translation_request_all SET status = 'pending', incoming = $2 WHERE trid = $1";
    private static final String FINISH_QUERY = "UPDATE translation_request_all SET status = 'finished', dict = $2 WHERE trid = $1";
    private static final String STATUS_QUERY = "SELECT status, COUNT(status) as count FROM translation_request_all GROUP BY status";

    public DatabaseServiceImpl(Vertx vertx, JsonObject config, Handler<AsyncResult<DatabaseService>> readyHandler) {
        PgConnectOptions connectOptions = new PgConnectOptions()
                .setHost(config.getString("host", "localhost"))
                .setPort(config.getInteger("port", 5432))
                .setDatabase(config.getString("db_name", ""))
                .setUser(config.getString("user", ""))
                .setPassword(config.getString("password", ""));
        PoolOptions poolOptions = new PoolOptions().setMaxSize(config.getInteger("jdbc_max_pool_size", 100));
        dbClient = PgPool.client(vertx, connectOptions, poolOptions);
        createTableTranslationRequest()
                .compose(f -> resetActiveStatus())
                .onSuccess(v -> {
                    log.info("Database service started");
                    readyHandler.handle(Future.succeededFuture(this));
                })
                .onFailure(cause -> readyHandler.handle(Future.failedFuture(cause)));
    }

    private Future<Void> createTableTranslationRequest() {
        return Future.future(promise ->
                dbClient.query(CREATE_TABLE_QUERY).execute()
                        .onSuccess(rowSet -> promise.complete())
                        .onFailure(promise::fail)
        );
    }

    private Future<Void> resetActiveStatus() {
        return Future.future(promise ->
                dbClient.query(RESET_ALL_QUERY).execute()
                        .onSuccess(rowSet -> promise.complete())
                        .onFailure(promise::fail)
        );
    }

    @Override
    public Future<Void> insertTranslationRequest(JsonObject translationRequest) {
        return Future.future(promise -> {
            List<Object> params = new ArrayList<>();

            params.add(translationRequest.getString("id"));
            params.add("pending");
            params.add(LocalDateTime.now());
            params.add(translationRequest.getJsonObject("data", new JsonObject()));
            params.add(translationRequest.getJsonObject("dict", new JsonObject()));
            params.add(translationRequest.getJsonObject("callback", new JsonObject()));

            dbClient.preparedQuery(INSERT_QUERY)
                    .execute(Tuple.from(params))
                    .onSuccess(rowSet -> promise.complete())
                    .onFailure(promise::fail);
        });
    }

    @Override
    public Future<JsonObject> nextTranslationRequest() {
        return Future.future(promise ->
                dbClient.query(NEXT_QUERY).execute()
                        .onSuccess(rowSet -> {
                            if (rowSet.rowCount() > 0) {
                                Row row = rowSet.iterator().next();
                                setTranslationRequestToActive(row.getString("trid"), ar -> {
                                    if (ar.succeeded()) {
                                        promise.complete(row.toJson());
                                    } else {
                                        promise.fail(ar.cause());
                                    }
                                });
                            } else {
                                promise.complete(new JsonObject());
                            }
                        })
                        .onFailure(promise::fail)
        );
    }

    private void setTranslationRequestToActive(String trId, Handler<AsyncResult<Void>> resultHandler) {
        dbClient.preparedQuery(SET_ACTIVE_QUERY).execute(Tuple.of(trId))
                .onSuccess(rowSet -> resultHandler.handle(Future.succeededFuture()))
                .onFailure(cause -> resultHandler.handle(Future.failedFuture(cause)));
    }

    @Override
    public Future<JsonObject> selectTranslationRequest(String trId) {
        return Future.future(promise ->
                dbClient.preparedQuery(SELECT_QUERY).execute(Tuple.of(trId))
                        .onSuccess(rowSet -> {
                            if (rowSet.iterator().hasNext()) {
                                Row row = rowSet.iterator().next();
                                promise.complete(row.toJson());
                            } else {
                                promise.complete(new JsonObject());
                            }
                        })
                        .onFailure(promise::fail)
        );
    }

    public Future<Void> deleteTranslationRequest(String trId) {
        return Future.future(promise ->
                dbClient.preparedQuery(DELETE_QUERY)
                        .execute(Tuple.of(trId))
                        .onSuccess(rowSet -> promise.complete())
                        .onFailure(promise::fail)
        );
    }

    @Override
    public Future<Void> resetTranslationRequest(String trId) {
        return Future.future(promise ->
                dbClient.preparedQuery(RESET_QUERY).execute(Tuple.of(trId, LocalDateTime.now().toString()))
                        .onSuccess(rowSet -> promise.complete())
                        .onFailure(promise::fail)
        );
    }

    @Override
    public Future<Void> finishTranslation(JsonObject request) {
        return Future.future(promise -> {
            String trId = request.getString("id");
            JsonObject dict = request.getJsonObject("dict", new JsonObject());
            // Should be updated with translations
            dbClient.preparedQuery(FINISH_QUERY).execute(Tuple.of(trId, dict))
                    .onSuccess(rowSet -> promise.complete())
                    .onFailure(promise::fail);
        });
    }

    @Override
    public Future<JsonObject> status() {
        return Future.future(promise ->
                dbClient.query(STATUS_QUERY).execute()
                        .onSuccess(rowSet -> {
                            JsonObject status = new JsonObject();
                            rowSet.forEach(row ->
                                    status.put(row.getString("status"), row.getInteger("count"))
                            );
                            promise.complete(status);
                        })
                        .onFailure(promise::fail)
        );
    }

}
