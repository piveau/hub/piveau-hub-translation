# Changelog

## Unreleased

## 2.1.0 (2022-10-08)

**Added:**
* Configuration option `keepFinalized`
* Optional delete finalized requests from db

**Changed:**
* DB service request eventbus timeout set to 5 min

## 2.0.24 (2022-03-25)

**Fixed:**
* Decrement snippet counter when snippets are reset 

## 2.0.23 (2022-03-24)

**Added:**
* Snippet counter to status

## 2.0.22 (2022-03-16)

**Fixed:**
* Date time order in query changed to ASC (the oldest first)

## 2.0.21 (2022-03-11)

**Changed:**
* Timeout checks active snippets instead of active requests

## 2.0.20 (2022-02-14)

**Changed:**
* Track active snippet durations

## 2.0.19 (2022-02-03)

**Fixed:**
* Active snippet counter incremented before sending (!)

## 2.0.18 (2022-02-03)

**Changed:**
* Use `isExpectMultipart()` instead checking content type

**Removed:**
* Some trace level log outputs

## 2.0.17 (2022-02-02)

**Fixed:**
* Future complete, also if no corresponding snippet in request found
* Form attribute array reading

## 2.0.16 (2022-02-02)

**Added:**
* Reset snippet when re-queued

**Fixed:**
* Misplaced log output

## 2.0.15 (2022-02-02)

**Changed:**
* Concurrent list handling 

**Added:**
* Attributes tracing in error callback

## 2.0.14 (2022-02-01)

**Changed:**
* Use vertx web client for eTranslation service

**Removed:**
* Separate eTranslation verticle

## 2.0.13 (2022-02-01)

**Added:**
* Snippet counter for quota
* Quota check

## 2.0.12 (2022-02-01)

**Fixed:**
* Error code for 'Translation timeout'
* Some log output

**Removed:**
* Snippet counter for quota
* Quota check

**Changed:**
* Status incomplete snippets count all snippets not completed 

## 2.0.11 (2022-01-31)

**Fixed:**
* eTranslation error callback attribute parsing

## 2.0.10 (2022-01-31)

**Added:**
* Status information about duration of active requests

## 2.0.9 (2022-01-29)

**Fixed:**
* Surprisingly requires synchronization in periodic timer

## 2.0.8 (2022-01-25)

**Fixed:**
* Disable circuit breaker timeout

## 2.0.7 (2022-01-25)

**Added:**
* Using a circuit breaker for specific failures
* Handling error -20028 (Concurrency quota exceeded)

**Changed:**
* DB status from 'sent' to 'active'

## 2.0.6 (2022-01-15)

**Changed:**
* Incoming translation log output to info level

## 2.0.5 (2022-01-14)

**Fixed:**
* eTranslation error callback parsing

## 2.0.4 (2022-01-13)

**Fixed:**
* Very slow eTranslation response time

## 2.0.3 (2022-01-13)

**Added:**
* Quota config value
* Snippet quota

**Changed:**
* Increased default snippet interval

## 2.0.2 (2022-01-12)

Skipped 2.0.1

**Changed:**
* Improved log output
* Singleton http client

## 2.0.0 (2022-01-12)

**Changed:**
* Everything

## 1.0.3 (2021-12-14)

**Added:**
* GET methods for callback endpoints

**Changed:**
* Minor config naming
* Order of db initialisation

## 1.0.2 (2021-12-08)

**Fixed:**
* Some build and deployment issues

## 1.0.1 (2021-12-07)

**Fixed**
* Worker status only for translation service verticle

## 1.0.0 (2021-10-22)

**Changed**
* New algorithms for interaction between TS and eTranslation.
* LICENSE.dms -> LICENSE.md
* Failed translation are now saved in a separate database table for further investigations.

**Removed**
*  A lot of unnecessary logs outputs (only commented out)

## 0.1.0 (2021-08-20)

**Added**
* Semantic Versioning
* LICENSE.dms

**Changed**
* Algorithm for handling translation snippets.
* New structure of modules.
